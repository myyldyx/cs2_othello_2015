#include "player.h"
#include "stdio.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    myboard = new Board();
    myside = side;
}

/*
 * Destructor for the player.
 */
Player::~Player() {      
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move* placeKey(Board* board0, Side side0)
{
  Move* move0 = new Move(0, 0);
  if (board0 -> checkMove(move0, side0)) return move0;
  Move* move1 = new Move(0, 7);
  if (board0 -> checkMove(move1, side0)) return move1;
  Move* move2 = new Move(7, 0);
  if (board0 -> checkMove(move2, side0)) return move2;
  Move* move3 = new Move(7, 7);
  if (board0 -> checkMove(move3, side0)) return move3;
  return NULL;
}

int ExistScore(Board* board0, Side side0)
{
  Side other = (side0 == BLACK) ? WHITE : BLACK;
  return board0 -> count(side0) - board0 ->count(other);
}

int MobilScore(Board* board0, Side side0)
{
  Side other = (side0 == BLACK) ? WHITE : BLACK;
  int n = 0;
  for (int i = 0; i < 8; i++)
    for (int j = 0; j < 8; j++)
      {
        Move * move0 = new Move(i, j);
	if (board0 -> checkMove(move0, other)) n += 1;
	delete move0;
      }  
  return 64 - n;
}

int MixScore(Board* board0, Side side0)
{
  int score = 0;
  score += MobilScore(board0, side0);
  score += ExistScore(board0, side0);
  return score;
}

int score(Board* board0, Side side0)
{
  int score = 0;
  Side other = (side0 == BLACK) ? WHITE : BLACK;  
  Move* move0 = new Move(0, 0);
  if (board0 -> checkMove(move0, other)) score += -20;
  Move* move1 = new Move(0, 7);
  if (board0 -> checkMove(move1, other)) score += -20;
  Move* move2 = new Move(7, 0);
  if (board0 -> checkMove(move2, other)) score += -20;
  Move* move3 = new Move(7, 7);
  if (board0 -> checkMove(move3, other)) score += -20;
  score += board0 -> checkKey(side0);
  return score;
}

int DoABminimax(Board* board0, Side side0, int n, Side myside, int a, int b, int t)
{
  Side other = (side0 == BLACK) ? WHITE : BLACK;
  if ((n == 0) or (!board0 -> hasMoves(side0)))
  {
    int score;
    if (t == 1) score = ExistScore(board0, side0);
    if (t == 2) score = MobilScore(board0, side0);
    if (t == 3) score = MixScore(board0, side0);
    return score;
  }
  if (side0 == myside) 
    {
      int max = -1000;
      for (int i = 0; i < 8; i++)
        for (int j = 0; j < 8; j++)
	  {
            Move* move0 = new Move(i, j);
            if (board0 -> checkMove(move0, side0))
	      {
                Board *board1 = board0 -> copy();
                board1 -> doMove(move0, side0);
                int val = DoABminimax(board1, other, n-1, myside, a, b, t);
                if (val > max) max = val; 
                if (max > a) a = max;
                if (b < a) break;
             }
          }
      return max;
    }
  else
    {
      int min = 64;
      for (int i = 0; i < 8; i++)
        for (int j = 0; j < 8; j++)
	  {
            Move* move0 = new Move(i, j);
            if (board0 -> checkMove(move0, side0))
	      {
                Board *board1 = board0 -> copy();
                board1 -> doMove(move0, side0);
                int val = DoABminimax(board1, other, n-1, myside, a, b, t);
                if (val < min)  min = val;
                if (min < b) b = min;
	        if (b < a) break; 
             }
          }
      return min;
    }  
}

Move* ABminimax(int step, Board* board0, Side side0, int n, Side myside,int a, int b, int t)
{
  Side other = (side0 == BLACK) ? WHITE : BLACK;
  int max = -64;
  Move *bmove = NULL;
  for (int i = 0; i < 8; i++)
    for (int j = 0; j < 8; j++)
      {
        Move* move0 = new Move(i, j);
        if (board0 -> checkMove(move0, side0))
	  {
            Board *board1 = board0 -> copy();
            board1 -> doMove(move0, side0);
            int val = DoABminimax(board1, other, n-1, myside, a, b, t);
            if (step <= 54) val += score(board1, myside);
            if (val > max)
	      {
                max = val;
                bmove = move0;
              }
	    if (max > a) a = max;
            if (b < a) break;
	  }
      }
   return bmove;
}

Move *OccupyCenter(Board* board0, Side side0)
{
  int max = -64;
  int n;
  Move * bmove = NULL;
  for (int i = 0; i < 8; i++)
    for (int j = 0; j < 8; j++)
      {
        Move* move0 = new Move(i, j);
        if (board0 -> checkMove(move0, side0))
	  {
            Board *board1 = board0 -> copy();
            board1 -> doMove(move0, side0);
            n = board1 -> countCenter(side0);
            n += score(board1, side0);
            if (n > max) 
	      {
                max = n;
                bmove = move0; 
              }
          } 
      }
  return bmove;
}


Move *Player::doMove(Move *opponentsMove, int msLeft) { 
    Side other = (myside == BLACK) ? WHITE : BLACK;
    myboard->doMove(opponentsMove, other);
    if (!myboard->hasMoves(myside))
      {
        return NULL;
      }
    Move *bestmove = NULL;
    int step = myboard -> count(myside) + myboard -> count(other);
    if (step < 40)
    {
      bestmove = placeKey(myboard, myside);
      if (!bestmove) bestmove = OccupyCenter(myboard, myside);
    }
    else if (step <= 50)
    {
      bestmove = placeKey(myboard, myside);
      if (!bestmove) bestmove = ABminimax(step, myboard, myside, 6, myside, -64, 64, 3);
    }
    else if (step <= 56)
    {
      bestmove = placeKey(myboard, myside);
      if (!bestmove) bestmove = ABminimax(step, myboard, myside, 6, myside, -64, 64, 1);
    }
    else
    {
      bestmove = ABminimax(step, myboard, myside, 8, myside, -64, 64, 1);
    }
    myboard -> doMove(bestmove, myside);
    return bestmove;
}
