#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    // int Player::DoMinimax(Board board0, Side side0, int n);
    // Move *Player::minimax(Board board0, Side side0, int n);
    Move *doMove(Move *opponentsMove, int msLeft);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;

private:
    Board *myboard;
    Side myside;
};

#endif
